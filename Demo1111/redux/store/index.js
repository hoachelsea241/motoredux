import { createStore, combineReducers } from "redux";
import motoReducer from '../reducers/motoReduccer';


const reducers = combineReducers({
    moto : motoReducer,
});

const initStore = () =>{ return createStore(reducers) };

export default initStore;


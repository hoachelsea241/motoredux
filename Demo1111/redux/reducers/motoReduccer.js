import { ADD_MOTO  } from "../actions/types";

const init = {
    listMoto:[]
}

const motoReducer = (state = init, action) => {     
    switch (action.type){
        case ADD_MOTO:
            return {
                ...state,
                listMoto: state.listMoto.concat(
                    {
                        id: Math.random(),
                        nameMoto: action.nameMoto,
                        colorMoto: action.colorMoto,
                        statusMoto: action.statusMoto,
                    }
                )

            };
        default:
            return state;
    }
}

export default motoReducer;
import { ADD_MOTO } from './types';
const addMoto = (name, color, status) => {
    return {
        type: ADD_MOTO,
        nameMoto: name,
        colorMoto: color,
        statusMoto: status,
    }
}
export default addMoto;
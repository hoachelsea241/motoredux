
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, FlatList, TextInput} from 'react-native';
import {connect} from 'react-redux';
import addMoto from './redux/actions/moto';

 class App extends Component{
  state = {
    motoName: '',
    motoColor: '',
  }

  changeText= (value) => {
    this.setState({
      motoName: value
      
    })
  }
  changeColor= (value) => {
    this.setState({
      motoColor: value
    })
  }
  addMotoTolist=()=> {
    this.props.addMoto(this.state.motoName, this.state.motoColor, true )
  }

  render() 
  {
    return (
      <View style={styles.container}>
      <Text> {this.state.motoName}</Text>
        <TextInput
        placeholder="name"
        onChangeText={this.changeText}
        />
        <TextInput
        placeholder="color"
        onChangeText={this.changeColor}
        />
        <Button
          onPress={this.addMotoTolist}
          title="add"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        />
        <FlatList
          data={this.props.ListMoto}
          renderItem={({item}) => <Text>{item.nameMoto + ' ' + item.colorMoto }</Text>}
        />
      </View>
    );
  }
}

const mapStateToProps =(state )=>{
  return{
    ListMoto: state.moto.listMoto
  }
};
const mapDispatchToProps = (dispatch)=>{
  return{
    addMoto: (nameMoto, colorMoto, statusMoto) => {
      dispatch(addMoto(nameMoto, colorMoto, statusMoto))
    }
  }
};

export default connect(mapStateToProps , mapDispatchToProps)(App);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React from 'react';
import initStore  from './redux/store';
import {Provider} from 'react-redux';

const store = initStore();

const RNApp = ()=>{
    return(
        <Provider store={store}>
            <App/>
        </Provider>
        
    )
}
AppRegistry.registerComponent(appName, () => RNApp);
